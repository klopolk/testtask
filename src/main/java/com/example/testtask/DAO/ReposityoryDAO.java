package com.example.testtask.DAO;

import com.example.testtask.entity.Product;
import org.hibernate.MultiIdentifierLoadAccess;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.List;

@Repository
@Transactional
public class ReposityoryDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public ReposityoryDAO() {
    }

    /**
     * Получение всех объектов из таблицы
     */
    public <T> List<T> getAll(Class<T> className) {
        String sql = "Select e from " + className.getName() + " e ";
        Session session = this.sessionFactory.getCurrentSession();
        Query<T> query = session.createQuery(sql, className);
        return query.getResultList();
    }

    /**
     * Получение объекта из таблицы по его Id
     *
     * @param className - Класс получаемого объекта
     * @param id        - id из таблицы, соответствующей сущности
     */
    public <T> T getById(Class<T> className, long id) {
        Session session = this.sessionFactory.getCurrentSession();
        MultiIdentifierLoadAccess<T> multi = session.byMultipleIds((className));
        List<T> entitys = multi.multiLoad(id);
        return entitys.get(0);
    }

    /**
     * обновление  объекта в таблице
     *
     * @param entity - entity объект ,который следует обновить
     */
    public <T> void update(T entity) {
        Session session = this.sessionFactory.getCurrentSession();
        session.merge(entity);
    }

    /**
     * Получение объектов из таблицы Product, типы которых имеются в списке-параметре
     *
     * @param idTypeChain - список id из таблицы Type, для фильтрации объектов
     */
    public List<Product> getAllWithFilter(List<Long> idTypeChain) {
        if (StringUtils.isEmpty(idTypeChain)) return Collections.emptyList();
        String idTypeChainString = idTypeChain.toString()
                .replace("[", "")
                .replace("]", "");

        String sql = String.format("select * from product where product.type_id in (%s) " +
                "AND id in ( select min(id) from product group by product.type_id)", idTypeChainString);
        Session session = this.sessionFactory.getCurrentSession();
        Query<Product> query = session.createNativeQuery(sql, Product.class);
        List<Product> listType = query.getResultList();

        return listType;
    }
}
