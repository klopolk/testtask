package com.example.testtask.request;


/**
 * запрос с сайта, включает следующие поля
 * automobileId - id автомобиля
 * productId - id продукта,который необходимо вставить в авто
 * isReplacement - флаг, подтверждающий замену продукта в случае обнаружения совпадения по типу
 */
public class AddProductRequest {
    private long automobileId;
    private long productId;
    private boolean isReplacement;

    public AddProductRequest(long automobileId, long productId, boolean isReplacement) {
        this.automobileId = automobileId;
        this.productId = productId;
        this.isReplacement = isReplacement;
    }

    public long getAutomobileId() {
        return automobileId;
    }

    public void setAutomobileId(long automobileId) {
        this.automobileId = automobileId;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }


    public boolean isReplacement() {
        return isReplacement;
    }

    public void setReplacement(boolean replacement) {
        isReplacement = replacement;
    }
}