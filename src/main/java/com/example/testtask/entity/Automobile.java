package com.example.testtask.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "automobile")
public class Automobile {
    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private List<Product> product;

    public List<Product> getProduct() {
        return product;
    }

    public void setProduct(List<Product> product) {
        this.product = product;
    }

    public Automobile() {
    }

    public Automobile(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Automobile(Long id, String name, List<Product> product) {
        this.id = id;
        this.name = name;
        this.product = product;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
