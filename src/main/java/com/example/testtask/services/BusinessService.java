package com.example.testtask.services;

import com.example.testtask.DAO.ReposityoryDAO;
import com.example.testtask.entity.Automobile;
import com.example.testtask.entity.Product;
import com.example.testtask.entity.Type;
import com.example.testtask.request.AddProductRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BusinessService {
    private Logger logger = LoggerFactory.getLogger(BusinessService.class);

    @Autowired
    private ReposityoryDAO reposityoryDAO;

    public String process(AddProductRequest request) {
        long autoId = request.getAutomobileId();
        long addProductId = request.getProductId();
        boolean isReplacement = request.isReplacement();

        Automobile auto = reposityoryDAO.getById(Automobile.class, autoId);
        Product addProduct = reposityoryDAO.getById(Product.class, addProductId);
        List<Product> includeProducts = auto.getProduct();

        boolean isContainAddProductType = false;
        int indexOfIncludeFindProduct = -1;
        boolean isParentHere = false;

        //определить есть ли в авто продукт с таким типом
        for (Product includeProduct : includeProducts) {
            if (includeProduct.getType().getName().equals(addProduct.getType().getName())) {
                isContainAddProductType = true;
                indexOfIncludeFindProduct = includeProducts.indexOf(includeProduct);
                break;
            }
            if (includeProduct.getType().getId() == addProduct.getType().getParent()) {
                isParentHere = true;
            }

        }
        logger.debug("isContainAddProductType ={} , indexOfIncludeFindProduct = {}, isParentHere = {}", isContainAddProductType, indexOfIncludeFindProduct, isParentHere);

        String rs;
        if (isContainAddProductType) {
            rs = getRsContainProduct(isReplacement, auto, addProduct, indexOfIncludeFindProduct);
        } else if (addProduct.getType().getParent() == null || isParentHere) {
            rs = getRsIncludeAllChain(auto, addProduct);
        } else {
            rs = getRsNoConditionsForInclude(addProduct);
        }

        logger.debug("response ={}", rs);
        return rs;
    }

    /**
     * К авто уже подключен продукт , по типу такой же как и добавляемый
     * - Если продукт такой же , то сообщение о наличии
     * - Если продукт другой , то при наличии подтверждения - заменить на новый,
     * при отсутсвтвии подтверждения - сообщение с необходимости подтверждения замены
     */
    private String getRsContainProduct(boolean isReplacement, Automobile auto, Product addProduct, int indexOfIncludeFindProduct) {
        String rs;
        List<Product> includeProducts = auto.getProduct();
        Product findProduct = includeProducts.get(indexOfIncludeFindProduct);
        if (findProduct.getName().equals(addProduct.getName())) {
            rs = String.format("К авто %s , id = %d уже подключен продукт %s , id = %d",
                    auto.getName(), auto.getId(), addProduct.getName(), addProduct.getId());
        } else {
            if (isReplacement) {// подтверждение замены одного продукта на другой
                includeProducts.set(indexOfIncludeFindProduct, addProduct);
                reposityoryDAO.update(auto);
                rs = String.format("В авто %s id = %d обновлен продукт типа %s на %s ,id нового продукта = %d",
                        auto.getName(), auto.getId(), addProduct.getType().getName(), addProduct.getName(), addProduct.getId());
            } else {
                rs = String.format("В авто %s Продукт типа %s уже подключен.\nПодтвердите замену подключенного продукта %s id = %d на продукт %s id = %d",
                        auto.getName(), addProduct.getType().getName(), findProduct.getName(), findProduct.getId(), addProduct.getName(), addProduct.getId());
            }
        }
        return rs;
    }

    /**
     * К авто уже подключен продукт , по типу являющийся предком подключаемого продукта,
     * Либо подключаемый продукт продукт является корневым
     * <p>
     * Подключается сам продукт и все продукты, тип которых требует наличия типа-родителя совпадаемого с
     * типом подключаемого продукта
     * <p>
     * Сообщение о подключении цепочки продуктов
     */
    private String getRsIncludeAllChain(Automobile auto, Product addProduct) {
        String rs;
        List<Product> includeProducts = auto.getProduct();
        ArrayList<Long> idTypeChain = new ArrayList<>();
        idTypeChain.add(addProduct.getType().getId());
        defineIdDependentTypes(idTypeChain, addProduct.getType().getChild());

        List<Product> addProductsChain = reposityoryDAO.getAllWithFilter(idTypeChain);
        includeProducts.addAll(addProductsChain);
        reposityoryDAO.update(auto);
        rs = "Подключены следующие продукты = " + addProductsChain;
        return rs;
    }

    /**
     * Не созданы условия для подключения .
     * Среди уже подключенных не имеется продукта с типом, являющиимся предком подключаемого продукта.
     * <p>
     * Сообщение о необходимости создания условия для подключения
     */
    private String getRsNoConditionsForInclude(Product addProduct) {
        String rs;
        Type parentType = reposityoryDAO.getById(Type.class, addProduct.getType().getParent());
        rs = String.format("Для подключения продукта типа %s сначала требуется подключить продукт типа %s ,id типа = %d"
                , addProduct.getType().getName(), parentType.getName(), parentType.getId());
        return rs;
    }

    /**
     * Определить id зависимых типов
     *
     * @param idTypeChain - список List<Long> , содержающий все id типов ,которые следует подключить вместе с запрашиваемым продуктом
     * @param idElement   - id элемента списка.
     */
    private void defineIdDependentTypes(ArrayList<Long> idTypeChain, Long idElement) {
        if (idElement != null) {
            Type type = reposityoryDAO.getById(Type.class, idElement);
            idTypeChain.add(type.getId());
            defineIdDependentTypes(idTypeChain, type.getChild());
        }
    }

}
