package com.example.testtask.controller;

import com.example.testtask.DAO.ReposityoryDAO;
import com.example.testtask.entity.Automobile;
import com.example.testtask.entity.Product;
import com.example.testtask.request.AddProductRequest;
import com.example.testtask.services.BusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/auto")
public class AutomobileController {

    @Autowired
    private ReposityoryDAO reposityoryDAO;

    @Autowired
    private BusinessService businessService;

    @GetMapping(value = "")
    public List<Automobile> getAllAuto() {
        List<Automobile> list = reposityoryDAO.getAll(Automobile.class);

        return list;
    }

    @GetMapping("/{id}")
    public Automobile getAutoById(@PathVariable long id) {
        Automobile auto = reposityoryDAO.getById(Automobile.class, id);
        return auto;
    }

    @GetMapping("/{id}/products")
    public List<Product> getProductByAutoId(@PathVariable long id) {
        Automobile auto = reposityoryDAO.getById(Automobile.class, id);
        return auto.getProduct();
    }

    @PostMapping("/product")
    public String addProduct(@RequestBody AddProductRequest request) {
        String rs = businessService.process(request);
        return rs;
    }
}